# Bowl Of Butter 
A Website made to highlight the rich history of flavored butter bowls within the Tampa Bay area. 

## How it works
Built as a Child Theme using the UnderStrap Theme Framework. Hosted via VPS Droplet within a docker container running nginx. 

DEMO-URL: `https://stevenrescigno-dev.com`

**IT DOES NOT LOAD THE PARENT THEMES CSS FILE(S)!** 

## Installation
1. Install the parent theme UnderStrap first: `https://github.com/holger1411/understrap`
   - IMPORTANT: If you download UnderStrap from GitHub make sure you rename the "understrap-master.zip" file to "understrap.zip" or you might have problems using this child theme!
1. Upload the understrap-child folder to your wp-content/themes directory
1. Go into your WP admin backend 
1. Go to "Appearance -> Themes"
1. Activate the UnderStrap Child theme
