<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<?php 

// args
$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'post',
);


// query
$the_query = new WP_Query( $args );

?>

<?php if( $the_query->have_posts() ): ?>

<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
	<div class="col-xl-3 col-md-3 service-box" id="post-<?php the_ID(); ?>" >
		<div class="feature-box ">
		<div class="feature-icon feature1"></div>
		<div class="feature-detail">
			<div class="product-image butterBowl-image-container"> 
			<a href="<?php the_permalink(); ?>"> 
				<img src="<?php the_field('image'); ?>" alt="<?php the_ID(); ?>"> 
			</a>
			</div>
			<div class="butterBowl-product-title"><?php the_field('name'); ?></div>
			<div class="ser-subtitle">From <?php the_field('price'); ?></div>
			<p class="butterBowl-description"><?php the_field('description'); ?></p>
			<ul class="butterBowl-ingredients">
				<?php the_field('ingredients'); ?>
			</ul>
		</div>
		</div>
	</div>	
	<?php endwhile; ?>

<?php endif; ?>

<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>





