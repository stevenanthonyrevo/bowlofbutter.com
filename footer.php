<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="butterBowl-wrapper" id="wrapper-footer">    
	<!-- FOOTER START -->
	<footer class="butterBowl-footer footer">
        <div class="container">
          <div class="footer-inner">
            <div class="footer-middle">
              <div class="row">
                <div class="col-xl-3 f-col">
                  <div class="footer-static-block"> <span class="opener plus"></span>
                    <div class="f-logo"> 
                        <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
						$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
						if ( has_custom_logo() ) {
								echo '<img class="butterBowl-footer-logo" src="'. esc_url( $logo[0] ) .'">';
						} else {
								echo '<h4>'. get_bloginfo( 'name' ) .'</h4>';
						} ?>
                    </div>
                    <div class="footer-block-contant">
                        <p><?php $site_description = get_bloginfo( 'description' ); echo $site_description ?></p>
                    </div>
                  </div>
                </div>
                <div class="col-xl-3 col-md-4 f-col">
				<?php if ( is_active_sidebar( 'home_footer_1' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area footer-static-block" role="complementary">
						<?php dynamic_sidebar( 'home_footer_1' ); ?>
					</div><!-- #primary-sidebar -->
				<?php endif; ?>
				</div>
				<div class="col-xl-3 col-md-4 f-col">
				<?php if ( is_active_sidebar( 'home_footer_2' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area footer-static-block" role="complementary">
						<?php dynamic_sidebar( 'home_footer_2' ); ?>
					</div><!-- #primary-sidebar -->
				<?php endif; ?>
                </div>
                <div class="col-xl-3 col-md-4 f-col">
				<?php if ( is_active_sidebar( 'home_footer_3' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area footer-static-block" role="complementary">
						<?php dynamic_sidebar( 'home_footer_3' ); ?>
					</div><!-- #primary-sidebar -->
				<?php endif; ?>
                </div>
              </div>
            </div>
            <hr>
            <div class="footer-bottom ">
              <div class="row mtb-30">
                <div class="col-lg-6 ">
                  <div class="copy-right ">© 2018 All Rights Reserved.</div>
                </div>
                <div class="col-lg-6 ">
                  <div class="footer_social pt-xs-15 center-sm">
					<?php if ( is_active_sidebar( 'home_social_1' ) ) : ?>
							<?php dynamic_sidebar( 'home_social_1' ); ?>
					<?php endif; ?>
                  </div>
                </div>
              </div>
              <hr>
              <div class="row align-center mtb-30 ">
                <div class="col-12 ">
                  <div class="site-link">
				  	<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container_class' => 'new_menu_class' ) ); ?>
                  </div>
                </div>
              </div>
              <div class="row align-center">
                <div class="col-12 ">
                  <div class="payment">
				  <?php if ( is_active_sidebar( 'home_footer_4' ) ) : ?>
							<?php dynamic_sidebar( 'home_footer_4' ); ?>
					<?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>
<script type="text/javascript">
	function myFunction() {
		var x = document.getElementById("myDIV");
		console.log(window.length)
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
		function myFunctiontwo() {
		var x = document.getElementById("myDIVtwo");
			console.log(window.length)
		console.log(x)
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
	function butterBowlNav() {
		var x = document.getElementById("myDIV");
		console.log(window.length)
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
		function myFunctiontwo() {
		var x = document.getElementById("myDIVtwo");
			console.log(window.length)
		console.log(x)
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
</script>
</html>

