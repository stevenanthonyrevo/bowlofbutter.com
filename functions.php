<?php
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );


function spaceone_widgets_init() {

	register_sidebar( array(
		'name'          => 'Widget Footer One',
		'id'            => 'home_footer_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="rounded title">',
		'after_title'   => '<span></span></h3>',
	) );

}
add_action( 'widgets_init', 'spaceone_widgets_init' );

function spacetwo_widgets_init() {

	register_sidebar( array(
		'name'          => 'Widget Footer Two',
		'id'            => 'home_footer_2',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="rounded title">',
		'after_title'   => '<span></span></h3>',
	) );

}
add_action( 'widgets_init', 'spacetwo_widgets_init' );

function spacethree_widgets_init() {

	register_sidebar( array(
		'name'          => 'Widget Footer Three',
		'id'            => 'home_footer_3',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="rounded title">',
		'after_title'   => '<span></span></h3>',
	) );

}
add_action( 'widgets_init', 'spacethree_widgets_init' );

function spacefour_widgets_init() {

	register_sidebar( array(
		'name'          => 'Widget Footer Four',
		'id'            => 'home_footer_4',
		'before_widget' => '<ul class="butterBowl-payment-icon">',
		'after_widget'  => '</ul>',
		'before_title'  => '<h3 class="rounded title">',
		'after_title'   => '<span></span></h3>',
	) );

}
add_action( 'widgets_init', 'spacefour_widgets_init' );

function social_widgets_init() {

	register_sidebar( array(
		'name'          => 'Widget Social',
		'id'            => 'home_social_1',
		'before_widget' => '<ul class="social-icon butterBowl-social">',
		'after_widget'  => '</ul>',
		'before_title'  => '<li><div>',
		'after_title'   => '</div></li>',
	) );

}
add_action( 'widgets_init', 'social_widgets_init' );

add_theme_support( 'custom-logo' );

function themename_custom_logo_setup() {
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );

function register_butterBowl_menus() {
    register_nav_menus(
      array(
        'secondary-menu' => __( 'Secondary Menu' ),
        'mobile-menu' => __( 'Mobile Menu' ),
        'footer-menu' => __( 'Footer Menu' ),
      )
    );
}

add_action( 'init', 'register_butterBowl_menus' );