<?php
/**
 * Template Name: Home
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

 <!-- BANNER STRAT -->
 <section class="butterBowl-slider">
	<div class="butterBowl-banner ">
		<div class="butterBowl-banner-1">
		<?php if( get_field('image') ): ?>
			<img class="butterBowl-img" src="<?php the_field('image'); ?>" alt="ButterBowl" />
		<?php endif; ?>
			<!-- <img class="butterBowl-img" src="http://localhost/BowlOfButter.com/wp-content/uploads/2018/12/banner-ButterBowl-01.jpg" alt="ButterBowl"> -->
		<div class="butterBowl-banner-detail">
			<div class="container">
			<div class="row">
				<div class="butterBowl-center">
				<div class="banner-detail-inner"> 
				<?php if( get_field('slogan') ): ?>
					<span class="slogan"><?php the_field('slogan'); ?></span>
				<?php endif; ?>
				<?php if( get_field('title') ): ?>
					<h1 class="butterBowl-banner-title"><?php the_field('title'); ?></h1>
				<?php endif; ?>
				<?php if( get_field('offer') ): ?>
					<span class="offer"><?php the_field('offer'); ?></span>
				<?php endif; ?>
				</div>
				<?php if( get_field('cta') ): ?>
				<a class="btn btn-color" href="<?php the_field('page-link'); ?>"><?php the_field('cta'); ?></a>
				<?php endif; ?>	
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>
</section>
<!-- BANNER END --> 

 <section class="ser-feature-block">
          <div class="container">
             <div class="row m-0">
              <div class="col-12 p-0">
                <div class="heading-part mb-30 mb-xs-15">
                   <h3 class="title">Speciality Bowls <span></span></h3>
                  <!-- <h2 class="main_title heading"><span>Speciality Bowls</span></h2> -->
                </div>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="center-xs">
              <div class="row">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php

						/*
						* Include the Post-Format-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Format name) and that will be used instead.
						*/
						get_template_part( 'loop-templates/content', get_post_format() );
						?>

					<?php endwhile; ?>

					<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

			<?php endif; ?>

			  
              </div>
            </div>
          </div>
        </section>


<?php get_footer(); ?>
