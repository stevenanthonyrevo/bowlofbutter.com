<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <title><?php the_title(); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">
    <meta name="keywords" content="">
    <meta name="robots" content="ALL">
    <meta name="Language" content="<?php get_locale() ?>">
    <meta name="GOOGLEBOT" content="NOARCHIVE">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="site" id="page">
  
	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">
	<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>
	<header class="butterBowl-header">
		<nav class="navbar navbar-expand-md navbar-dark bg-primary butterBowl">
			<div class="header-bottom"> 
		<?php if ( 'container' == $container ) : ?>
			<div class="container butterBowl-container" >
		<?php endif; ?>
					<div class="position-r butterBowl-row">
					<div class="butterBowl-header-logo col-xl-2 col-lg-3 col-lgmd-20per position-initial">
						<!-- Your site title as branding in the menu -->
						<?php if ( ! has_custom_logo() ) { ?>

							<div class="sidebar-menu-dropdown home"> 
							<?php if ( is_front_page() && is_home() ) : ?>
							
								<a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url" class="btn-sidebar-menu-dropdown "><?php bloginfo( 'name' ); ?></a>
							
							<?php else : ?>

								<a class="navbar-brand btn-sidebar-menu-dropdown" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url" class="btn-sidebar-menu-dropdown "><?php bloginfo( 'name' ); ?></a>

							<?php endif; ?>
							</div>

						<?php } else {
							the_custom_logo();
						} ?><!-- end custom logo -->
					</div>
					<div class="col-xl-6 col-lg-6 col-lgmd-60per">
						<div class="bottom-inner">
							<div class="position-r">
								<div class="nav_sec position-r">
									<div class="mobilemenu-title mobilemenu closemenu">
										<span>Menu</span>
										<i class="fa fa-bars pull-right" onclick="butterBowlNav()"></i>
									</div>
									 <div class="mobilemenu-content" id="myDIV" style="display: none;">
								  	<!-- The WordPress Menu goes here -->
									<?php wp_nav_menu(
											array(
												'theme_location'  => 'primary',
												'container_class' => '',
												'container_id'    => '',
												'menu_class'      => 'nav navbar-nav',
												'fallback_cb'     => '',
												'menu_id'         => 'main-menu',
												'depth'           => 2,
												'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
											)
									); ?>
                                </div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 col-lg-3 col-lgmd-20per">
                        <div class="right-side float-left-xs header-right-link">
                            <div class="right-side">
							  <div class="help-num">Need Help? : 03 233 455 55</div>
							</div>
                        </div>
                      </div>
				</div>
			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>
			</div>
		</nav><!-- .site-navigation -->
	</header>
									
                
	</div><!-- #wrapper-navbar end -->
